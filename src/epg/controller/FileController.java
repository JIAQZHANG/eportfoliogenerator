package epg.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import epg.model.EPortfolioModel;
import epg.error.ErrorHandler;
import epg.file.EPortfolioFileManager;
import epg.view.EPortfolioGeneratorView;
import static epg.StartupConstants.*;
import epg.model.Page;

/**
 * This class serves as the controller for all file toolbar operations, driving
 * the loading and saving of slide shows, among other things.
 *
 * @author McKilla Gorilla & JIAQI
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    public static boolean saveWork = false;

    // THE APP UI
    private EPortfolioGeneratorView ui;

    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private EPortfolioFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file
     * being edited.
     *
     * @param initEPortfolioIO The object that will be reading and writing slide
     * show data.
     */
    public FileController(EPortfolioGeneratorView initUI, EPortfolioFileManager initEPortfolioIO) {
        // NOTHING YET
        saved = true;
        ui = initUI;
        slideShowIO = initEPortfolioIO;
    }

    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewEPortfolioRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                EPortfolioModel ePortfolio = ui.getEPortfolio();
                ePortfolio.reset();
                saved = false;
                ui.reloadSiteToolbarPane();
                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // MAKE SURE THE TITLE CONTROLS ARE ENABLED
                //ui.reloadTitleControls();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadEPortfolioRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING);
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we
     * already know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveEPortfolioRequest() {
        try {
            // GET THE SLIDE SHOW TO SAVE
            EPortfolioModel slideShowToSave = ui.getEPortfolio();

            // SAVE IT TO A FILE
            slideShowIO.saveEPortfolio(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
            return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
            return false;
        }
    }

    /**
     * This method shows the current slide show in a separate window.
     */
//    public void handleViewEPortfolioRequest() throws IOException {
//      //Orignal statements
////	EPortfolioViewer viewer = new EPortfolioViewer(ui);
////	viewer.startEPortfolio();
//
//        //CREATE ALL NECESSARY FILES
//        File htmlFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/index.html");
//        htmlFile.getParentFile().mkdir();
//        File cssFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/css/slideshow_style.css");
//        cssFile.getParentFile().mkdir();
//        File jsFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/js/Slideshow.js");
//        jsFile.getParentFile().mkdir();
//
//        //Copy json file
//        File jsonFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/js/" + ui.getEPortfolio().getTitle() + ".json");
//        jsonFile.getParentFile().mkdir();
//        File jsonSource = new File("./data/slide_shows/" + ui.getEPortfolio().getTitle() + ".json");
//        Files.copy(jsonSource.toPath(), jsonFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
//
//        //Copy images
//        BufferedImage image;
//        for (Page slide : ui.getEPortfolio().getPages()) {
//            image = ImageIO.read(new File(slide.getImagePath() + slide.getImageFileName()));
//            File imgFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/img/" + slide.getImageFileName());
//            imgFile.getParentFile().mkdir();
//            ImageIO.write(image, "jpg", imgFile);
//        }
//
//        //COPY ICONS
//        image = ImageIO.read(new File(PATH_ICONS + "Previous.png"));
//        File iconFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/icons/Previous.png");
//        iconFile.getParentFile().mkdir();
//        ImageIO.write(image, "png", iconFile);
//        image = ImageIO.read(new File(PATH_ICONS + "Next.png"));
//        iconFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/icons/Next.png");
//        iconFile.getParentFile().mkdir();
//        ImageIO.write(image, "png", iconFile);
//        image = ImageIO.read(new File(PATH_ICONS + "Play.png"));
//        iconFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/icons/Play.png");
//        iconFile.getParentFile().mkdir();
//        ImageIO.write(image, "png", iconFile);
//        image = ImageIO.read(new File(PATH_ICONS + "Pause.png"));
//        iconFile = new File("./sites/" + ui.getEPortfolio().getTitle() + "/icons/Pause.png");
//        iconFile.getParentFile().mkdir();
//        ImageIO.write(image, "png", iconFile);
//
//        BufferedWriter bw;
//        //Write in CSS file
//        bw = new BufferedWriter(new FileWriter(cssFile));
//        bw.write("h1{\n"
//                + "    padding:5px;\n"
//                + "    background-color: #B266FF;\n"
//                + "    font-family: \"Comic Sans MS\", cursive, sans-serif;\n"
//                + "    font-size:  300%;\n"
//                + "}\n"
//                + "img.center{\n"
//                + "    height:700px;\n"
//                + "    width:1000px;\n"
//                + "    padding:5px;\n"
//                + "}\n"
//                + "p{\n"
//                + "    font-size:200%;\n"
//                + "    padding:10px;\n"
//                + "    background-color: #B266FF;\n"
//                + "    font-family: \"Comic Sans MS\", cursive, sans-serif;\n"
//                + "} \n"
//                + "#slideshow {\n"
//                + "    text-align:center;\n"
//                + "    background-color: #606060;\n"
//                + "} \n"
//                + "#controls {\n"
//                + "    text-align:center;\n"
//                + "}\n"
//                + "input.prevBtn{\n"
//                + "    background-image: url(\"../icons/Previous.png\");\n"
//                + "    background-position: center;\n"
//                + "    background-repeat: no-repeat;\n"
//                + "    width: 50px;\n"
//                + "    height: 50px;  \n"
//                + "    margin: 0px 10px 0px 10px;\n"
//                + "}\n"
//                + "input.nextBtn{\n"
//                + "    background-image: url(\"../icons/Next.png\");\n"
//                + "    background-position: center;\n"
//                + "    background-repeat: no-repeat;\n"
//                + "    width: 50px;\n"
//                + "    height: 50px;   \n"
//                + "    margin: 0px 10px 0px 10px;\n"
//                + "}\n"
//                + "input.playBtn{\n"
//                + "    background-image: url(\"../icons/Play.png\");\n"
//                + "    background-position: center;\n"
//                + "    background-repeat: no-repeat;\n"
//                + "    width: 50px;\n"
//                + "    height: 50px;\n"
//                + "    margin: 0px 10px 0px 10px;\n"
//                + "}\n"
//                + "input.pauseBtn{\n"
//                + "    background-image: url(\"../icons/Pause.png\");\n"
//                + "    background-position: center;\n"
//                + "    background-repeat: no-repeat;\n"
//                + "    width: 50px;\n"
//                + "    height: 50px;    \n"
//                + "    margin: 0px 10px 0px 10px;\n"
//                + "}");
//        bw.close();
//
//        //Write in Html File
//        bw = new BufferedWriter(new FileWriter(htmlFile));
//        bw.write("<html>");
//        bw.write("<head>");
//        bw.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + "css/slideshow_style.css" + "\">");
//        bw.write("<script src=\"js/Slideshow.js\" type=\"text/javascript\"></script>");
//        bw.write("</head>");
//        bw.write("<body onload =\"load()\" >");
//        bw.write("<div id=\"slideshow\" >");
//
////        bw.write("<h1>" + ui.getEPortfolio().getTitle() + "</h1>");
////        bw.write("<img class=\"center\" src= \"img/" + ui.getEPortfolio().getSlides().get(0).getImageFileName() + "\">");
////        bw.write("<p>"+ ui.getEPortfolio().getSlides().get(0).getCaption()+ "</p>");
//        bw.write("</div>");
//
//        bw.write("<div id=\"controls\">");
//        bw.write("<input type=\"button\" class=\"prevBtn\"  onClick=\"getPrevious()\">"
//                + "<input type=\"button\" class=\"playBtn\"  onClick=\"play()\">"
//                + "<input type=\"button\" class=\"nextBtn\"  onClick=\"getNext()\">");
//        bw.write("</div>");
//
//        bw.write("</body>");
//        bw.write("</html>");
//        bw.close();
//
//        bw = new BufferedWriter(new FileWriter(jsFile));
//        bw.write("\n"
//                + "var xmlhttp;\n"
//                + "var file;\n"
//                + "var json;\n"
//                + "var i = 0;\n"
//                + "var timer;\n"
//                + "\n"
//                + "function load() {\n"
//                + "    xmlhttp = new XMLHttpRequest();\n"
//                + "    file = \"js/" + ui.getEPortfolio().getTitle() + ".json\";\n"
//                + "    xmlhttp.onreadystatechange = function () {\n"
//                + "        if (xmlhttp.readyState === 4) {\n"
//                + "            if (xmlhttp.status === 0 || xmlhttp.status === 200) {\n"
//                + "                json = JSON.parse(xmlhttp.responseText);\n"
//                + "                var code = \"\";\n"
//                + "                code += \"<h1>\" + json.title + \"</h1>\\n\" +\n"
//                + "                        \"<img class=\\\"center\\\" src= \\\"img/\" + json.slides[0].image_file_name + \"\\\">\\n\" +\n"
//                + "                        \"<p>\" + json.slides[0].caption + \"</p>\\n\";\n"
//                + "                document.getElementById(\"slideshow\").innerHTML = code;\n"
//                + "            }\n"
//                + "        }\n"
//                + "    }\n"
//                + "    xmlhttp.open(\"GET\", file, true);\n"
//                + "    xmlhttp.send();\n"
//                + "}\n"
//                + "\n"
//                + "function getNext() {\n"
//                + "    var out = \"\";\n"
//                + "    if (json.slides[i + 1] !== undefined) {\n"
//                + "        out += \"<h1>\" + json.title + \"</h1>\\n\" +\n"
//                + "                \"<img class=\\\"center\\\" src= \\\"img/\" + json.slides[i + 1].image_file_name + \"\\\">\\n\" +\n"
//                + "                \"<p>\" + json.slides[i + 1].caption + \"</p>\\n\";\n"
//                + "        document.getElementById(\"slideshow\").innerHTML = out;\n"
//                + "        i++;\n"
//                + "    }\n"
//                + "    else {\n"
//                + "        alert(\"The end of slide show!\");\n"
//                + "    }\n"
//                + "}\n"
//                + "\n"
//                + "function getPrevious() {\n"
//                + "    var out = \"\";\n"
//                + "    if (json.slides[i - 1] !== undefined) {\n"
//                + "        out += \"<h1>\" + json.title + \"</h1>\\n\" +\n"
//                + "                \"<img class=\\\"center\\\" src= \\\"img/\" + json.slides[i - 1].image_file_name + \"\\\">\\n\" +\n"
//                + "                \"<p>\" + json.slides[i - 1].caption + \"</p>\\n\";\n"
//                + "        document.getElementById(\"slideshow\").innerHTML = out;\n"
//                + "        i--;\n"
//                + "    }\n"
//                + "    else {\n"
//                + "        alert(\"It is the first slide!\");\n"
//                + "    }\n"
//                + "}\n"
//                + "\n"
//                + "function play() {\n"
//                + "    document.getElementById(\"controls\").innerHTML =\n"
//                + "            \"<input type=\\\"button\\\" class=\\\"prevBtn\\\"  onClick=\\\"getPrevious()\\\">\" +\n"
//                + "            \"<input type=\\\"button\\\" class=\\\"pauseBtn\\\"  onClick=\\\"pause()\\\">\" +\n"
//                + "            \"<input type=\\\"button\\\" class=\\\"nextBtn\\\"  onClick=\\\"getNext()\\\">\";\n"
//                + "\n"
//                + "    timer = setInterval(function () {\n"
//                + "        if (json.slides[i + 1] !== undefined) {\n"
//                + "            document.getElementById(\"slideshow\").innerHTML = \"<h1>\" + json.title + \"</h1>\\n\" +\n"
//                + "                    \"<img class=\\\"center\\\" src= \\\"img/\" + json.slides[i + 1].image_file_name + \"\\\">\\n\" +\n"
//                + "                    \"<p>\" + json.slides[i + 1].caption + \"</p>\\n\";\n"
//                + "            i++;\n"
//                + "        }\n"
//                + "        else {\n"
//                + "            document.getElementById(\"slideshow\").innerHTML = \"<h1>\" + json.title + \"</h1>\\n\" +\n"
//                + "                    \"<img class=\\\"center\\\" src= \\\"img/\" + json.slides[0].image_file_name + \"\\\">\\n\" +\n"
//                + "                    \"<p>\" + json.slides[0].caption + \"</p>\\n\";\n"
//                + "            i = 0;\n"
//                + "        }\n"
//                + "\n"
//                + "    }, 3000);\n"
//                + "\n"
//                + "\n"
//                + "}\n"
//                + "\n"
//                + "\n"
//                + "\n"
//                + "function pause() {\n"
//                + "    clearInterval(timer);\n"
//                + "    document.getElementById(\"controls\").innerHTML =\n"
//                + "            \"<input type=\\\"button\\\" class=\\\"prevBtn\\\"  onClick=\\\"getPrevious()\\\">\" +\n"
//                + "            \"<input type=\\\"button\\\" class=\\\"playBtn\\\"  onClick=\\\"play()\\\">\" +\n"
//                + "            \"<input type=\\\"button\\\" class=\\\"nextBtn\\\" onClick=\\\"getNext()\\\">\";\n"
//                + "}\n"
//                + "\n"
//                + "\n"
//                + "function alertfunction() {\n"
//                + "    alert(\"Page is loaded\");\n"
//                + "}");
//        bw.close();
//
//        //Show web view
//        SiteWebView webView = new SiteWebView(htmlFile.toURI().toURL().toString());
//        webView.startWebView();
//
//    }

    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        ui.promptToSaveUI();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            EPortfolioModel slideShow = ui.getEPortfolio();
            slideShowIO.saveEPortfolio(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_EPORTFOLIOS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                EPortfolioModel slideShowToLoad = ui.getEPortfolio();
                slideShowIO.loadEPortfolio(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSiteToolbarPane();
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}
