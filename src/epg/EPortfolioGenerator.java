/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

import static epg.StartupConstants.ICON_WINDOW_LOGO;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.TITLE_WINDOW;
import epg.file.EPortfolioFileManager;
import epg.view.EPortfolioGeneratorView;
import java.io.File;
import java.net.URL;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author JIAQI ZHANG
 */
public class EPortfolioGenerator extends Application{

     // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    EPortfolioFileManager fileManager = new EPortfolioFileManager();
    
    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    EPortfolioGeneratorView ui = new EPortfolioGeneratorView(fileManager);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        // SET THE WINDOW ICON
	String imagePath = PATH_ICONS + ICON_WINDOW_LOGO;
	File file = new File(imagePath);
	
	// GET AND SET THE IMAGE
	URL fileURL = file.toURI().toURL();
	Image windowIcon = new Image(fileURL.toExternalForm());
	primaryStage.getIcons().add(windowIcon);
        
        ui.startUI(primaryStage, TITLE_WINDOW);
    
    }
    
}
