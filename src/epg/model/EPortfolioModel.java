package epg.model;

import static epg.StartupConstants.DEFAULT_EPORTFOLIO_TITLE;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import epg.view.EPortfolioGeneratorView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioModel {
    EPortfolioGeneratorView ui;
    String title;
    ObservableList<Page> pages;
    Page selectedPage;
    
    public EPortfolioModel(EPortfolioGeneratorView initUI) {
	ui = initUI;
	pages = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isPageSelected() {
	return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage) {
	return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
    public Page getSelectedPage() {
	return selectedPage;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedPage(Page initSelectedPage) {
	selectedPage = initSelectedPage;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no pages and a default title.
     */
    public void reset() {
	pages.clear();
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = DEFAULT_EPORTFOLIO_TITLE;
	selectedPage = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption Caption for the slide image to add.
     */
    public void addPage() {
	Page slideToAdd = new Page();
	pages.add(slideToAdd);
	ui.reloadSiteToolbarPane();
    }

    /**
     * Removes the currently selected slide from the slide show and
     * updates the display.
     */
    public void removeSelectedPage() {
	if (isPageSelected()) {
	    pages.remove(selectedPage);
	    selectedPage = null;
	    ui.reloadSiteToolbarPane();
	}
    }

}