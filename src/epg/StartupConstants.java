package epg;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * 
 * @author McKilla Gorilla & _____________
 */
public class StartupConstants {

//    public static String PROPERTY_TYPES_LIST = "property_types.txt";
//    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
//    public static String UI_PROPERTIES_FILE_NAME_Finnish = "properties_FI.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_EPORTFOLIOS = PATH_DATA + "slide_shows/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_EPORTFOLIO_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "/epg/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioGeneratorStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "ApplicationIcon.png";
    public static String ICON_NEW_EPORTFOLIO = "New.png";
    public static String ICON_LOAD_EPORTFOLIO = "Load.png";
    public static String ICON_SAVE_EPORTFOLIO = "Save.png";
    public static String ICON_SAVE_AS_EPORTFOLIO = "Save_As.png";
    public static String ICON_VIEW_EPORTFOLIO = "View.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "Add.png";
    public static String ICON_REMOVE_PAGE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";

    // UI SETTINGS
    public static String    DEFAULT_PAGE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_EPORTFOLIO_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_EPORTFOLIO_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_SITE_TOOLBAR = "slide_site_toolbar";
    public static String    CSS_CLASS_PAGE_EDIT_VIEW = "page_edit_view";
    public static String    CSS_CLASS_SELECTED_PAGE_EDIT_VIEW = "selected_page_edit_view";
    public static String    CSS_CLASS_FILE_TOOLBAR_PANE = "file_toolbar_pane";
    public static String    CSS_CLASS_LANGUAGE_DIALOG = "language_dialog";
    public static String    CSS_CLASS_TITLE_PANE = "title_pane";
    public static String    CSS_CLASS_VIEWER_TOP_PANE = "viewer_top_pane";
    public static String    CSS_CLASS_VIEWER_BOTTOM_PANE = "viewer_bottom_pane";
    public static String    CSS_CLASS_WORKPLACE = "workplace";
    public static String    CSS_CLASS_SCROLL_PANE = "scroll_pane";
    public static String    CSS_CLASS_TITLE_HBOX = "title_hbox";
    public static String    CSS_CLASS_TITLE_PAGE_LABEL = "page_label";
    public static String    CSS_CLASS_PAGE_EDITOR_CONTROLS_VBOX = "page_editor_controls_vbox";
    // UI LABELS
    public static String    LABEL_EPORTFOLIO_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_SELECTION_PROMPT = "Select a Language:";
    public static String    OK_BUTTON_TEXT = "OK";
    
     // TITLE FOR WINDOW TITLE BAR
    public static String    TITLE_WINDOW = "ePortfolio Generator";
    
    // APPLICATION TOOLTIPS FOR BUTTONS
    public static String    TOOLTIP_NEW_EPORTFOLIO ="Create a new ePortfolio";
    public static String    TOOLTIP_LOAD_EPORTFOLIO = "Load an existing ePortfolio";
    public static String    TOOLTIP_SAVE_EPORTFOLIO = "Save current ePortfolio";
    public static String    TOOLTIP_SAVE_AS_EPORTFOLIO = "Save current ePortfolio as another file";
    public static String    TOOLTIP_VIEW_EPORTFOLIO = "View current ePortfolio";
    public static String    TOOLTIP_EXIT = "Exit the Application";
    public static String    TOOLTIP_ADD_PAGE = "Add a new Page";
    public static String    TOOLTIP_REMOVE_PAGE = "Remove selected Page";
    
    // DEFAULT VALUES
    public static String    DEFAULT_PAGE_NAME = "New Page";
    public static String    DEFAULT_EPORTFOLIO_TITLE = "New ePortfolio";
    
    // UI LABELS
    public static String    LABEL_PAGENAME = "Page name:";
    //LABEL_SLIDESHOW_TITLE,
}
