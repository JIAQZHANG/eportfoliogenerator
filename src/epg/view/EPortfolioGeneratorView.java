package epg.view;

import static epg.StartupConstants.*;
import epg.controller.EPortfolioEditController;
import epg.controller.FileController;
import epg.error.ErrorHandler;
import epg.file.EPortfolioFileManager;
import epg.model.EPortfolioModel;
import epg.model.Page;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * This class provides the User Interface for this application, providing
 * controls and the entry points for creating, loading, saving, editing, and
 * viewing slide shows.
 *
 * @author McKilla Gorilla & JIAQI ZHANG
 */
public class EPortfolioGeneratorView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane epgPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newEPortfolioButton;
    Button loadEPortfolioButton;
    Button saveEPortfolioButton;
    Button saveAsEPortfolioButton;
    Button viewEPortfolioButton;
    Button exitButton;

    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox pageEditToolbar;
    Button addPageButton;
    Button removePageButton;
//    Button moveSlideUpButton;
//    Button moveEPortfolioButton;

    //SET UP OF TITLE CONTROLLOR
    TextField titleField = new TextField();
    Label titleLabel = new Label("ePortfolio Title: ");
    Button setTitleBtn = new Button("Set Title");
    Label pageLabel = new Label("Pages:");

    // AND THIS WILL GO IN THE CENTER
    ScrollPane pagesEditorScrollPane;
    VBox siteToolbar;

    //THESE GO TO THE RIGHT
    TabPane modeTabPane;
    Tab pageEditorWorkspaceTab;
    Tab siteViewerWorkspaceTab;
    BorderPane pageEditorWorkspace;
    BorderPane siteViewerWorkspace;

    Button addHeadingButton;
    Button addParagraphButton;
    Button addListButton;
    Button addImageButton;
    Button addVideoButton;
    Button addSlideshowButton;
    VBox pageEditorControlsVbox;

    Label pageTitleLabel;
    TextField pageTitleTextField;
    Label studentNameLabel;
    TextField studentNameTextField;
    Label layoutLabel;
    Label colorLabel;
    Label fontLabel;
    ComboBox layoutComboBox;
    ComboBox colorComboBox;
    ComboBox fontComboBox;
    Label bannerImageLabel;
    Button bannerImageButton;
    Label footerLabel;
    TextField footerTextField;
    VBox pageSettingVbox;
    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    EPortfolioModel ePortfolio;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    EPortfolioFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;

    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private EPortfolioEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public EPortfolioGeneratorView(EPortfolioFileManager initFileManager) {
        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;

        // MAKE THE DATA MANAGING MODEL
        ePortfolio = new EPortfolioModel(this);

        // WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
        errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public EPortfolioModel getEPortfolio() {
        return ePortfolio;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     *
     * @param initPrimaryStage The window for this application.
     *
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
        // FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
        workspace = new HBox();
        workspace.getStyleClass().add(CSS_CLASS_WORKPLACE);
        // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        pageEditToolbar = new VBox();
        pageEditToolbar.getStyleClass().add(CSS_CLASS_EPORTFOLIO_EDIT_VBOX);
        addPageButton = this.initChildButton(pageEditToolbar, ICON_ADD_PAGE, TOOLTIP_ADD_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        removePageButton = this.initChildButton(pageEditToolbar, ICON_REMOVE_PAGE, TOOLTIP_REMOVE_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
//	moveSlideUpButton = this.initChildButton(pageEditToolbar,	ICON_MOVE_UP,	    TOOLTIP_MOVE_UP,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
//	moveEPortfolioButton = this.initChildButton(pageEditToolbar,	ICON_MOVE_DOWN,	    TOOLTIP_MOVE_DOWN,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);

        // AND THIS WILL GO IN THE CENTER
        siteToolbar = new VBox();
        pagesEditorScrollPane = new ScrollPane(siteToolbar);
        siteToolbar.getStyleClass().add(CSS_CLASS_SITE_TOOLBAR);
	//initTitleControls();

        // NOW PUT THESE TWO IN THE WORKSPACE
        workspace.getChildren().add(pageEditToolbar);
        workspace.getChildren().add(pagesEditorScrollPane);

        initModeTabPane();
    }

    private void initEventHandlers() {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(this, fileManager);
        newEPortfolioButton.setOnAction(e -> {
            fileController.handleNewEPortfolioRequest();
        });
        loadEPortfolioButton.setOnAction(e -> {
            fileController.handleLoadEPortfolioRequest();
        });
        saveEPortfolioButton.setOnAction(e -> {
            fileController.handleSaveEPortfolioRequest();
        });
        viewEPortfolioButton.setOnAction(e -> {
            fileController.handleSaveEPortfolioRequest();
//            try {
//                fileController.handleViewEPortfolioRequest();
//            } catch (IOException ex) {
//                Logger.getLogger(EPortfolioGeneratorView.class.getName()).log(Level.SEVERE, null, ex);
//            }
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });

        // THEN THE PAGE SHOW EDIT CONTROLS
        editController = new EPortfolioEditController(this);
        addPageButton.setOnAction(e -> {
            editController.processAddPageRequest();
            fileController.markAsEdited();
        });
        removePageButton.setOnAction(e -> {
            editController.processRemovePageRequest();
            fileController.markAsEdited();
        });

        setTitleBtn.setOnAction(e -> {
            ePortfolio.setTitle(titleField.getText());
            fileController.markAsEdited();
        });
        
        
        addHeadingButton.setOnAction(e ->{
            AddComponentDialog addDialog = new AddComponentDialog(this);
            addDialog.addHeadingDialog();
        });
        addParagraphButton.setOnAction(e ->{
            AddComponentDialog addDialog = new AddComponentDialog(this);
            addDialog.addParagraphDialog();
        });
        addListButton.setOnAction(e ->{
            AddComponentDialog addDialog = new AddComponentDialog(this);
            addDialog.addListDialog();
        });
        addImageButton.setOnAction(e ->{
            AddComponentDialog addDialog = new AddComponentDialog(this);
            addDialog.addImageDialog();
        });
        addVideoButton.setOnAction(e ->{
            AddComponentDialog addDialog = new AddComponentDialog(this);
            addDialog.addVideoDialog();
        });
        addSlideshowButton.setOnAction(e ->{
            AddComponentDialog addDialog = new AddComponentDialog(this);
            addDialog.addSlideshowDialog();
        });
        
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_FILE_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        newEPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_EPORTFOLIO, TOOLTIP_NEW_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadEPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_EPORTFOLIO, TOOLTIP_LOAD_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO, TOOLTIP_SAVE_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        saveAsEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_AS_EPORTFOLIO, TOOLTIP_SAVE_AS_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        viewEPortfolioButton = initChildButton(fileToolbarPane, ICON_VIEW_EPORTFOLIO, TOOLTIP_VIEW_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        epgPane = new BorderPane();
        epgPane.setTop(fileToolbarPane);
        primaryScene = new Scene(epgPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    /**
     * Updates the enabled/disabled status of all toolbar buttons.
     *
     * @param saved
     */
    public void updateToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        epgPane.setCenter(workspace);

        // NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
        saveEPortfolioButton.setDisable(saved);
        saveAsEPortfolioButton.setDisable(saved);
        viewEPortfolioButton.setDisable(false);

        updatePageEditToolbarControls();
    }

    public void updatePageEditToolbarControls() {
        // AND THE SLIDESHOW EDIT TOOLBAR
        addPageButton.setDisable(false);
        boolean slideSelected = ePortfolio.isPageSelected();
        removePageButton.setDisable(!slideSelected);
//	moveSlideUpButton.setDisable(!slideSelected);
//	moveEPortfolioButton.setDisable(!slideSelected);	
    }

    /**
     * Uses the slide show data to reload all the components for slide editing.
     *
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSiteToolbarPane() {
        siteToolbar.getChildren().clear();
        //Print Slide Show Title Controls
        titleField.setText(ePortfolio.getTitle());
        HBox titleHbox = new HBox();
        titleHbox.getChildren().addAll(titleLabel, titleField, setTitleBtn);
        siteToolbar.getChildren().add(titleHbox);
        siteToolbar.getChildren().add(pageLabel);
        titleHbox.getStyleClass().add(CSS_CLASS_TITLE_HBOX);
        pageLabel.getStyleClass().add(CSS_CLASS_TITLE_PAGE_LABEL);

        for (Page page : ePortfolio.getPages()) {
            PageNameEditView pageEditor = new PageNameEditView(page);
            if (ePortfolio.isSelectedPage(page)) {
                pageEditor.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
            } else {
                pageEditor.getStyleClass().add(CSS_CLASS_SELECTED_PAGE_EDIT_VIEW);
            }
            siteToolbar.getChildren().add(pageEditor);
            pageEditor.setOnMousePressed(e -> {
                ePortfolio.setSelectedPage(page);
                this.reloadWorkspace();
                this.reloadSiteToolbarPane();
            });
        }
        updatePageEditToolbarControls();
    }

    public void reloadWorkspace() {
        if (ePortfolio.isPageSelected()) {
            if (workspace.getChildren().contains(modeTabPane)) {
                workspace.getChildren().remove(modeTabPane);
            }
            workspace.getChildren().add(modeTabPane);
            HBox.setHgrow(modeTabPane, Priority.ALWAYS);
        } else {
            workspace.getChildren().remove(modeTabPane);
        }
    }

    public void initModeTabPane() {
        
        modeTabPane = new TabPane();
        modeTabPane.getStyleClass().add("mode_tab_pane");
        pageEditorWorkspaceTab = new Tab("Page Editor Workspace");
        siteViewerWorkspaceTab = new Tab("Site Viewer Workspace");
        pageEditorWorkspace = new BorderPane();
        pageEditorWorkspace.getStyleClass().add("page_editor_workspace");
        siteViewerWorkspace = new BorderPane();
        pageEditorWorkspaceTab.setContent(pageEditorWorkspace);
        siteViewerWorkspaceTab.setContent(siteViewerWorkspace);
        modeTabPane.getTabs().add(pageEditorWorkspaceTab);
        modeTabPane.getTabs().add(siteViewerWorkspaceTab);
        initPageEditorWorkspace();
    }

    public void initPageEditorWorkspace() {
        addHeadingButton = new Button("Add Heading");
        addParagraphButton = new Button("Add Paragraph");
        addListButton = new Button("Add List");
        addImageButton = new Button("Add Image");
        addVideoButton = new Button("Add Video");
        addSlideshowButton = new Button("Add Slideshow");
        pageEditorControlsVbox = new VBox();
        pageEditorControlsVbox.getChildren().addAll(addHeadingButton, addParagraphButton,
                addListButton, addImageButton, addVideoButton, addSlideshowButton);
        pageEditorControlsVbox.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_CONTROLS_VBOX);
        pageEditorWorkspace.setLeft(pageEditorControlsVbox);

        pageTitleLabel = new Label("Page Title:");
        pageTitleTextField = new TextField();
        studentNameLabel = new Label("Student Name:");
        studentNameTextField = new TextField();
        layoutLabel = new Label("Layout:");
        colorLabel = new Label("Color:");
        fontLabel = new Label("Font:");
        bannerImageLabel = new Label("Banner Image:");
        bannerImageButton = new Button("Choose Banner Image");
        footerLabel = new Label("Footer:");
        footerTextField = new TextField();
        ObservableList<String> layoutChoices = FXCollections.observableArrayList();
        ObservableList<String> colorChoices = FXCollections.observableArrayList();
        ObservableList<String> fontChoices = FXCollections.observableArrayList();
        for (int i = 0; i < 5; i++) {
            layoutChoices.add("layout_" + (i + 1));
            colorChoices.add("color_" + (i + 1));
            fontChoices.add("font_" + (i + 1));
        }
        layoutComboBox = new ComboBox(layoutChoices);
        layoutComboBox.setValue(layoutChoices.get(0));
        colorComboBox = new ComboBox(colorChoices);
        colorComboBox.setValue(colorChoices.get(0));
        fontComboBox = new ComboBox(fontChoices);
        fontComboBox.setValue(fontChoices.get(0));

        pageSettingVbox = new VBox();
        pageSettingVbox.getStyleClass().add("page_setting_vbox");
        pageSettingVbox.getChildren().addAll(new HBox(20, pageTitleLabel, pageTitleTextField, studentNameLabel, studentNameTextField),
                //new HBox(20,studentNameLabel, studentNameTextField),
                new HBox(20, layoutLabel, layoutComboBox, colorLabel, colorComboBox, fontLabel, fontComboBox),
                new HBox(20, bannerImageLabel, bannerImageButton),
                new HBox(20, footerLabel, footerTextField));
        pageEditorWorkspace.setTop(pageSettingVbox);
    }

//    private void initTitleControls() {
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String labelPrompt = props.getProperty(LABEL_SLIDESHOW_TITLE);
//	titlePane = new FlowPane();
//	titleLabel = new Label(labelPrompt);
//	titleTextField = new TextField();
//        titlePane.getStyleClass().add(CSS_CLASS_TITLE_PANE);
//	
//	titlePane.getChildren().add(titleLabel);
//	titlePane.getChildren().add(titleTextField);
//	
//	String titlePrompt = props.getProperty(LanguagePropertyType.LABEL_SLIDESHOW_TITLE);
//	titleTextField.setText(titlePrompt);
//	
//	titleTextField.textProperty().addListener(e -> {
//	    ePortfolio.setTitle(titleTextField.getText());
//	});
//    }
//    
//    public void reloadTitleControls() {
//	siteToolbar.getChildren().add(titlePane);
//	titleTextField.setText(ePortfolio.getTitle());
//    }
    public void promptToSaveUI() {
        Stage stage = new Stage();
        Label label = new Label("Save current slide show?");
        Button btYes = new Button("Yes");
        Button btNo = new Button("No");
        HBox btnHbox = new HBox(15, btYes, btNo);
        btnHbox.setAlignment(Pos.CENTER);

        btYes.setOnMouseClicked(e -> {
            FileController.saveWork = true;
            stage.close();
        });

        btNo.setOnAction(e -> stage.close());

        VBox vbox = new VBox(20, label, btnHbox);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        stage.setScene(new Scene(vbox));
        stage.showAndWait();

    }

}
