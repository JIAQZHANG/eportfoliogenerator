package epg.view;

import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import static epg.StartupConstants.*;
import epg.controller.ImageSelectionController;
import epg.error.ErrorHandler;
import static epg.file.EPortfolioFileManager.SLASH;
import epg.model.Page;

/**
 * This UI component has the controls for editing a single page
 in a page show, including controls for selected the page image
 and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PageNameEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Page page;
    
    // CONTROLS FOR EDITING THE CAPTION
    Label pageNameLabel;
    Label nameLabel;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initPage data for initializing values./
     * 
     * @param initPage The page to be edited by this component.
     */
    public PageNameEditView(Page initPage) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
        
	// KEEP THE SLIDE FOR LATER
	page = initPage;

	// SETUP THE CAPTION CONTROLS
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
	pageNameLabel = new Label(LABEL_PAGENAME);
	nameLabel = new Label();
	nameLabel.setText(page.getName());

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(pageNameLabel);
	getChildren().add(nameLabel);

	nameLabel.textProperty().addListener(e -> {
	    String text = nameLabel.getText();
	    page.setName(text);	    
	});
    }

}