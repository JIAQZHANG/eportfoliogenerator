/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author JIAQI ZHANG
 */
public class AddComponentDialog {

    Stage stage;
    Scene scene;
    VBox root;
    Button okButton;
    Button cancelButton;
    HBox buttonHbox;
    
    HBox listHbox;
    ObservableList<HBox> listHBox;
    int i;
    
    private EPortfolioGeneratorView ui;

    /**
     * This constructor keeps the UI for later.
     */
    public AddComponentDialog(EPortfolioGeneratorView initUI) {
        this.listHBox = FXCollections.observableArrayList();
        ui = initUI;
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        buttonHbox = new HBox(okButton, cancelButton);
        buttonHbox.setAlignment(Pos.CENTER);
        buttonHbox.setSpacing(10);
        buttonHbox.setPadding(new Insets(5, 0, 5, 0));
        stage = new Stage();
        root = new VBox();
        scene = new Scene(root);
    }

    void addHeadingDialog() {
        Label headingLabel = new Label("Heading: ");
        headingLabel.setFont(new Font("Arial", 18));
        TextField headingTextField = new TextField();
        headingTextField.setFont(new Font(16));
        headingTextField.setPrefSize(400, 30);
        headingTextField.setFont(new Font("Arial", 18));
        VBox vBox = new VBox();
        vBox.getChildren().addAll(headingLabel, headingTextField);
        root.getChildren().addAll(vBox, buttonHbox);

        okButton.setOnAction(e -> {

        });
        cancelButton.setOnAction(e -> {
            stage.close();
        });

        stage.setTitle("Add Heading");
        stage.setScene(scene);
        stage.showAndWait();

    }

    void addParagraphDialog() {
        Label fontLabel = new Label("Font:");
        fontLabel.setFont(new Font("Arial", 18));
        ComboBox fontComboBox = new ComboBox();
        for (int i = 0; i < 5; i++) {
            fontComboBox.getItems().add(ui.fontComboBox.getItems().get(i));
        }
        fontComboBox.setValue(fontComboBox.getItems().get(0));
        Label paragraphLabel = new Label("Paragraph: ");
        paragraphLabel.setFont(new Font("Arial", 18));
        final TextArea paragraphTextArea = new TextArea();
        paragraphTextArea.setPrefSize(400, 250);
        paragraphTextArea.setWrapText(true);
        paragraphTextArea.setFont(new Font(16));
        VBox vBox = new VBox();
        vBox.getChildren().addAll(fontLabel, fontComboBox, paragraphLabel, paragraphTextArea);
        root.getChildren().addAll(vBox, buttonHbox);

        okButton.setOnAction(e -> {

        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });

        stage.setTitle("Add Paragraph");
        stage.setScene(scene);
        stage.showAndWait();

    }

    void addListDialog() {
        Label listLabel = new Label("List:");
        listLabel.setFont(new Font("Arial", 16));
        listLabel.setPadding(new Insets(5,0,5,0));
        root.getChildren().add(listLabel);
        loadListItem();
        

        stage.setTitle("Add List");
        stage.setScene(scene);
        stage.showAndWait();
    }

    void addImageDialog() {
        
    }

    void addVideoDialog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void addSlideshowDialog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void loadListItem() {
        Label itemLabel;
        TextField itemTextField;
        Button removeButton;
        Button addButton;
        for (i = 0; i < 2; i++) {
            itemLabel = new Label("Item: ");
            itemTextField = new TextField();
            removeButton = new Button("Remove");
            listHBox.add(new HBox(itemLabel, itemTextField, removeButton));

            removeButton.setOnAction(e->{
                listHBox.remove(i);
            });
            
        }
        addButton = new Button("Add Item");
        addButton.setOnAction(e->{
            listHBox.add(new HBox(new Label("Item:"), new TextField(), new Button("Remove")));
        });
        root.getChildren().addAll(addButton, buttonHbox);

        okButton.setOnAction(e -> {

        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });
    }

}
